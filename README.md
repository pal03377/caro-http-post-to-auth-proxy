# Caro HTTP POST to Auth Proxy


## About

This is a demo repository showcasing a very simple HTTP GET to HTTP Auth Proxy.
This kind of thing might be useful if you are in a situation where you need to
authenticate to a service via HTTP Auth, but the WebView you're using doesn't
support HTTP Auth.

## Security (!)

This is a very simple example, and is not intended to be used in production.
You should use a more secure method of storing your credentials if possible. 
This might however not always be an option and / or security doesn't matter.
In these cases it might be fine to use this method.

## Usage
Use Python 3:
```
python -m venv venv
source venv/bin/activate # Windows PowerShell: .\venv\Scripts\Activate.ps1
pip install -r requirements.txt
python server.py
```

Now you have a local debugging server listening on port 5000 (127.0.0.1 host only).
It will proxy all requests to the URL specified in the `PROXIED_SERVER` environment
variable. If the `PROXIED_SERVER` environment variable is not set, it will default
to `https://authenticationtest.com/`.
They provide a nice HTTP Auth test URL.

You set the HTTP Auth username and password using the GET parameters `__c_username` 
and `__c_password`. These GET parameters will not be passed on. They will be stored
in your Flask session for the current user.

Here's an example URL to test it out:
`http://127.0.0.1:5000/HTTPAuth/?__c_username=user&__c_password=pass`

The website will look a bit broken, but that's just because of CORS rules.

## License
MIT, see LICENSE.md