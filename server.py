import os
from flask import Flask, request, session
import requests
import secrets

app = Flask(__name__)
app.secret_key = secrets.token_urlsafe(256)

PROXIED_SERVER = os.environ.get("PROXIED_SERVER", "https://authenticationtest.com/")


@app.route('/<path:p>', methods=['GET', 'POST'])
def proxy_request(p):
    # Warning: It's not really that great to have username
    # and password in the request args. Only use this
    # for testing purposes or if there's really no other
    # way to do it.
    url = PROXIED_SERVER + p
    params = dict(request.args)
    if '__c_username' not in params and 'username' not in session:
        return "Missing __c_username parameter for HTTP Auth", 400
    username = params.pop('__c_username', session.get('username'))
    session['username'] = username
    if '__c_password' not in params:
        return "Missing __c_password parameter for HTTP Auth", 400
    password = params.pop('__c_password', session.get('password'))
    session['password'] = password
    if request.method == 'GET':
        r = requests.get(
            url, 
            params=params,
            headers=request.headers,
            auth=(username, password)
        )
    else:
        r = requests.post(
            url,
            data=request.data,
            params=params,
            headers=request.headers,
            auth=(username, password)
        )
    return r.text


@app.route('/', methods=['GET', 'POST'])
def proxy_root():
    return proxy_request('')

if __name__ == '__main__':
    app.run(
        host='127.0.0.1',
        port=5000,
        debug=True,
    )